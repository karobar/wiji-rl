package tjp.wiji_object.location;

import com.google.common.testing.EqualsTester;
import org.junit.Test;

public class CoordinatesTest {
    @Test
    public void testEquality() {
        new EqualsTester()
            .addEqualityGroup(new Coordinates(0,1), new Coordinates(0,1))
            .addEqualityGroup(new Coordinates(1,0));
    }
}
