package tjp.wiji_object.location;

import org.junit.Test;
import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.drawing.ObjectGeometry;
import tjp.wiji_object.drawing.PrecedenceClass;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji_object.objects.MapRelativeItem;
import tjp.wiji_object.objects.MergeClass;

import java.awt.*;
import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class ObjectLocatorTest {
    private static final ObjectGeometry TEST_WALL_GEOMETRY = new ObjectGeometry(
            Color.WHITE, Color.BLACK, Graphic.AMPERSAND);
    private static final ObjectGeometry TEST_FLOOR_GEOMETRY = new ObjectGeometry(
            Color.WHITE, Color.BLACK, Graphic.UNDERLINE);

    private static final GameObject.Factory TEST_FLOOR_FACTORY = new GameObject.Factory(
            "test floor", TEST_FLOOR_GEOMETRY, false, false);

    private static final GameObject.Factory TEST_WALL_FACTORY = new GameObject.Factory(
            "test wall", TEST_WALL_GEOMETRY, true, false);

    private static final GameObject SAMPLE_WALL = new GameObject(
            "test wall", TEST_WALL_GEOMETRY, true, false);

    private static final GameObject SAMPLE_FLOOR = new GameObject(
            "test floor", TEST_FLOOR_GEOMETRY, false, false);

    protected abstract Builder getBuilder();

    private Locator getSampleHorizontalWall() {
        return getBuilder().buildRectangle(
                TEST_WALL_FACTORY, TEST_FLOOR_FACTORY,
                5,3).translate(0,1);
    }

    private Locator getSampleVerticalWall() {
        return getBuilder().buildRectangle(
                TEST_WALL_FACTORY, TEST_FLOOR_FACTORY,
                5,3).translate(1,0);
    }

    private static class TestRelItem implements MapRelativeItem {
        @Override
        public Color getForeColor() {
            return Color.WHITE;
        }

        @Override
        public Optional<Color> getBackColor() {
            return Optional.empty();
        }

        @Override
        public Graphic getImgChar() {
            return Graphic.LOWERCASE_R;
        }

        @Override
        public boolean isBlocking() {
            return false;
        }

        @Override
        public TestRelItem copy() {
            return new TestRelItem();
        }

        @Override
        public boolean similarTo(Object other) {
            return this.equals(other);
        }

        @Override
        public MergeClass get() {
            return MergeClass.UNMERGE;
        }
    }

    private final MapRelativeItem TEST_ITEM = new TestRelItem();

    protected abstract Locator makeDut(MapRelativeItem item, Coordinates coords);

    @Test
    public void testGetFinalOutput() {
        final Locator dut = makeDut(TEST_ITEM, new Coordinates(1,1));

        assertTrue(dut.getFinalOutput(1, 1).isPresent());
    }


    private void assertTile(Locator symDiff, int x, int y, GameObject similarObject) {
        Collection<MapRelativeItem> basicWall = symDiff.getMapRelativeItems(x,y).get();
        assertEquals(1, basicWall.size());
        assertTrue(similarObject.similarTo(basicWall.iterator().next()));
    }

    @Test
    public void testSymmetricDifference() {
        Locator symDiff = getSampleHorizontalWall().symmetricDifference(getSampleVerticalWall());

        Optional<Collection<MapRelativeItem>> itemsAtZeroZero = symDiff.getMapRelativeItems(0,0);
        assertTrue(itemsAtZeroZero.isPresent());
        assertTrue(itemsAtZeroZero.get().isEmpty());
        assertTile(symDiff, 1, 0, SAMPLE_WALL);
        assertTile(symDiff, 1, 1, SAMPLE_WALL);
        assertTile(symDiff, 1, 2, SAMPLE_FLOOR);
        assertTile(symDiff, 2, 1, SAMPLE_FLOOR);
        assertTile(symDiff, 2, 2, SAMPLE_FLOOR);
    }

    @Test
    public void testSymmetricDifferenceSwitchOverlapping() {
        Locator symDiff = getSampleVerticalWall().symmetricDifference(getSampleHorizontalWall());

        Optional<Collection<MapRelativeItem>> itemsAtZeroZero = symDiff.getMapRelativeItems(0,0);
        assertTrue(itemsAtZeroZero.isPresent());
        assertTrue(itemsAtZeroZero.get().isEmpty());

        assertTile(symDiff, 1, 0, SAMPLE_WALL);
        assertTile(symDiff, 1, 1, SAMPLE_WALL);
        assertTile(symDiff, 1, 2, SAMPLE_FLOOR);
        assertTile(symDiff, 2, 1, SAMPLE_FLOOR);
        assertTile(symDiff, 2, 2, SAMPLE_FLOOR);
    }

    static final GameObject TEST_OBJECT = new GameObject(
            "Test Object",
            new ObjectGeometry(Color.GRAY, Color.RED, Graphic.AT_SYMBOL),
            false,
            false,
            PrecedenceClass.DEFAULT);


    static final GameObject FLOOR = new GameObject(
            "Test FLOOR",
            new ObjectGeometry(Color.DARK_GRAY, Color.BLUE, Graphic.AT_SYMBOL),
            false,
            false,
            PrecedenceClass.FLOOR);
    static final GameObject TEST_KEY_INFO = new GameObject(
            "Test Key Info",
            new ObjectGeometry(Color.GRAY, null, Graphic.AT_SYMBOL),
            false,
            false,
            PrecedenceClass.KEY_INFORMATION);

    @Test
    public void testGetFinalOutputBasic() {
        Locator trivial = makeDut(TEST_OBJECT,new Coordinates(0,0));

        assertEquals(new ImageRepresentation(Color.GRAY,Color.RED,Graphic.AT_SYMBOL),
                trivial.getFinalOutput(0,0).get());
    }

    @Test
    public void testGetFinalOutputConflict() {
        final Coordinates coordinates = new Coordinates(0,0);

        Locator trivial = makeDut(TEST_OBJECT,  coordinates);
        trivial = trivial.makeNewWithAdded(FLOOR, coordinates);

        assertEquals(new ImageRepresentation(Color.GRAY,Color.RED,Graphic.AT_SYMBOL),
                trivial.getFinalOutput(0,0).get());
    }

    @Test
    public void testGetFinalOutputConflictEmptyBackcolor() {
        final Coordinates coordinates = new Coordinates(0,0);

        Locator trivial = makeDut(TEST_KEY_INFO,  coordinates);
        trivial = trivial.makeNewWithAdded(FLOOR, coordinates);

        assertEquals(new ImageRepresentation(Color.GRAY,Color.BLUE,Graphic.AT_SYMBOL),
                trivial.getFinalOutput(0,0).get());
    }

    @Test
    public void testGetFinalOutputWithOverhang() {
        final Coordinates coordinates = new Coordinates(0,0);

        Locator trivial = makeDut(TEST_KEY_INFO,  coordinates);
        trivial = trivial.makeNewWithAdded(FLOOR, coordinates);
        trivial = trivial.makeNewWithAdded(
                new GameObject(
                        "Test overhang",
                        new ObjectGeometry(Color.BLUE, null, Graphic.ASTERISK),
                        false,
                        false,
                        PrecedenceClass.OVERHANG)
                , coordinates);

        // backcolor is pulled from the key info's forecolor
        assertEquals(
                new ImageRepresentation(Color.BLUE,Color.GRAY,Graphic.ASTERISK),
                trivial.getFinalOutput(0,0).get());
    }
}
