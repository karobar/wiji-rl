package tjp.wiji_object.location;

import com.google.common.collect.ImmutableSet;
import com.google.common.graph.Graph;
import org.junit.Ignore;
import org.junit.Test;
import tjp.wiji.representations.Graphic;
import tjp.wiji_object.drawing.ObjectGeometry;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji_object.objects.MapRelativeItem;
import tjp.wiji_object.objects.MergeClass;
import tjp.wiji_object.objects.Mergeable;

import java.awt.*;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@Ignore
public class ImmutableGraphLocatorTest extends ObjectLocatorTest {
//    private static final ObjectGeometry TEST_WALL_GEOMETRY = new ObjectGeometry(
//            Color.WHITE, Color.BLACK, Graphic.AMPERSAND);
    private static final ObjectGeometry TEST_FLOOR_GEOMETRY = new ObjectGeometry(
            Color.WHITE, Color.BLACK, Graphic.UNDERLINE);

    private static final GameObject.Factory TEST_FLOOR_FACTORY = new GameObject.Factory(
            "test floor", TEST_FLOOR_GEOMETRY, false, false);

    @Override
    protected Builder getBuilder() {
        return ImmutableGraphLocator.BUILDER;
    }

    @Override
    protected Locator makeDut(MapRelativeItem item, Coordinates coords) {
        return new ImmutableGraphLocator(item, coords);
    }
//
//    private static final GameObject.Factory TEST_WALL_FACTORY = new GameObject.Factory(
//            "test wall", TEST_WALL_GEOMETRY, true, false);
//
//    private static final GameObject SAMPLE_WALL = new GameObject(
//            "test wall", TEST_WALL_GEOMETRY, true, false);
//
//    private static final GameObject SAMPLE_FLOOR = new GameObject(
//            "test floor", TEST_FLOOR_GEOMETRY, false, false);
//
//
//    private static final ObjectLocator HORIZONTAL = BuildingUtils.buildRectangle(
//            TEST_WALL_FACTORY, TEST_FLOOR_FACTORY,
//            5,3).translate(0,1);
//
//    private static final ObjectLocator VERTICAL = BuildingUtils.buildRectangle(
//            TEST_WALL_FACTORY, TEST_FLOOR_FACTORY,
//            5,3).translate(1,0);

    private static class TestRelItem implements MapRelativeItem {
        @Override
        public Color getForeColor() {
            return Color.WHITE;
        }

        @Override
        public Optional<Color> getBackColor() {
            return Optional.empty();
        }

        @Override
        public Graphic getImgChar() {
            return Graphic.LOWERCASE_R;
        }

        @Override
        public boolean isBlocking() {
            return false;
        }

        @Override
        public TestRelItem copy() {
            return new TestRelItem();
        }

        @Override
        public boolean similarTo(Object other) {
            return this.equals(other);
        }

        @Override
        public MergeClass get() {
            return MergeClass.UNMERGE;
        }
    }

    private final MapRelativeItem TEST_ITEM = new TestRelItem();


//    private void assertTile(ObjectLocator symDiff, int x, int y, GameObject similarObject) {
//        Collection<MapRelativeItem> basicWall = symDiff.getMapRelativeItems(x,y).get();
//        assertEquals(1, basicWall.size());
//        assertTrue(similarObject.similarTo(basicWall.iterator().next()));
//    }
//
//    @Test
//    public void testSymmetricDifference() {
//        ObjectLocator symDiff = HORIZONTAL.symmetricDifference(VERTICAL);
//
//        Optional<Collection<MapRelativeItem>> itemsAtZeroZero = symDiff.getMapRelativeItems(0,0);
//        assertTrue(itemsAtZeroZero.isPresent());
//        assertTrue(itemsAtZeroZero.get().isEmpty());
//        assertTile(symDiff, 1, 0, SAMPLE_WALL);
//        assertTile(symDiff, 1, 1, SAMPLE_WALL);
//        assertTile(symDiff, 1, 2, SAMPLE_FLOOR);
//        assertTile(symDiff, 2, 1, SAMPLE_FLOOR);
//        assertTile(symDiff, 2, 2, SAMPLE_FLOOR);
//    }

//    @Test
//    public void testHasBlockingObjectFloor() throws TileNotFoundException {
//        final Coordinates coords = new Coordinates(1,1);
//
//        final ObjectLocator dut = new ImmutableGraphObjectLocator(ImmutableMap.of(
//                TEST_FLOOR_FACTORY.make(), coords));
//
//        assertFalse(dut.hasBlockingObject(coords));
//    }

    private static final class TestMergeable implements Mergeable {
        private final MergeClass delegate;

        TestMergeable(final MergeClass mergeClass) {
            this.delegate = mergeClass;
        }

        @Override
        public MergeClass get() {
            return delegate;
        }
    }

    @Test
    public void testMergeSimilarValues() {
        final Set<Mergeable> setA = ImmutableSet.of(new TestMergeable(MergeClass.FLOOR));
        final Set<Mergeable> setB = ImmutableSet.of(new TestMergeable(MergeClass.WALL));

        final Graph<Mergeable> output = ImmutableGraphLocator.mergeSimilarValues(
            new Coordinates(0, 0), setA, setB);

        assertEquals(3, output.nodes().size());
    }
}