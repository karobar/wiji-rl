package tjp.wiji_object.location;

import com.google.common.collect.ImmutableMap;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.MapRelativeItem;


public class ImmutableMultimapLocatorTest extends ObjectLocatorTest {
    @Override
    protected Builder getBuilder() {
        return ImmutableMultimapLocator.BUILDER;
    }

    @Override
    protected Locator makeDut(MapRelativeItem item, Coordinates coords) {
        return new ImmutableMultimapLocator(ImmutableMap.of(item, coords));
    }
}