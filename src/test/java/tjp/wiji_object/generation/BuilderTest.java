package tjp.wiji_object.generation;

import tjp.wiji_object.drawing.ObjectGeometry;
import tjp.wiji_object.exceptions.TileNotFoundException;
import junit.framework.TestCase;
import tjp.wiji_object.location.ImmutableMultimapLocator;
import tjp.wiji_object.location.Locator;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji.representations.Graphic;

import java.awt.*;

public class BuilderTest extends TestCase {
    private static final ObjectGeometry TEST_WALL_GEOMETRY = new ObjectGeometry(
            Color.WHITE, Color.BLACK, Graphic.AMPERSAND);
    private static final ObjectGeometry TEST_FLOOR_GEOMETRY = new ObjectGeometry(
            Color.WHITE, Color.BLACK, Graphic.UNDERLINE);

    private static final GameObject.Factory TEST_FLOOR_FACTORY = new GameObject.Factory(
            "test floor", TEST_FLOOR_GEOMETRY, false, false);

    private static final GameObject.Factory TEST_WALL_FACTORY = new GameObject.Factory(
            "test wall", TEST_WALL_GEOMETRY, true, false);

    public void testBuildWall() throws TileNotFoundException {
        final Builder dut = new Builder(ImmutableMultimapLocator::new);
        final Locator output = dut.buildRectangle(
                TEST_WALL_FACTORY, TEST_WALL_FACTORY, 3,3);
//
//        assertEquals(1, output.getMapRelativeItems(0,0).get().size());
//        assertEquals(1, output.getMapRelativeItems(1,0).get().size());
//        assertEquals(1, output.getMapRelativeItems(2,0).get().size());
    }

//    public void testBuildRectWalls() throws TileNotFoundException {
//        final ObjectLocator output = BuildingUtils.buildRectangle(
//                TEST_WALL_FACTORY, TEST_WALL_FACTORY, 3,3);
//
//        assertEquals(1, output.getMapRelativeItems(0,0).get().size());
//        assertEquals(1, output.getMapRelativeItems(1,0).get().size());
//        assertEquals(1, output.getMapRelativeItems(2,0).get().size());
//
//        assertEquals(1, output.getMapRelativeItems(2,1).get().size());
//
//        assertEquals(1, output.getMapRelativeItems(0,2).get().size());
//        assertEquals(1, output.getMapRelativeItems(1,2).get().size());
//        assertEquals(1, output.getMapRelativeItems(2,2).get().size());
//
//        assertEquals(1, output.getMapRelativeItems(0,1).get().size());
//    }
//
//    public void testBuildFloors() throws TileNotFoundException {
//        final ObjectLocator output = BuildingUtils.buildRectangle(
//                TEST_WALL_FACTORY, TEST_FLOOR_FACTORY, 3,3);
//
//        assertEquals(1, output.getMapRelativeItems(1,1).get().size());
//    }
}