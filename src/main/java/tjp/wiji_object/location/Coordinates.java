package tjp.wiji_object.location;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.errorprone.annotations.Immutable;
import tjp.wiji_object.objects.MergeClass;
import tjp.wiji_object.objects.Mergeable;

@Immutable
public class Coordinates implements Mergeable {
    private final int x;
    private final int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (getClass() != other.getClass()) return false;
        final Coordinates otherCoords = (Coordinates) other;
        return Objects.equal(this.x, otherCoords.x)
                && Objects.equal(this.y, otherCoords.y);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.x, this.y);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("x", x)
            .add("y", y)
            .toString();
    }

    public Coordinates translate(final Coordinates translationVector) {
        return new Coordinates(
                x + translationVector.x,
                y + translationVector.y);
    }

    @Override
    public MergeClass get() {
        return MergeClass.UNMERGE;
    }
}
