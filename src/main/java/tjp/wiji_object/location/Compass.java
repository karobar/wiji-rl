/*
 * Copyright 2013 Travis Pressler

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   * 
   * Compass.java
*/
package tjp.wiji_object.location;

/**
 * An enumerated type for helping a human make sense of movement instructions
 * without referring to a bunch of integers.
 * 
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public enum Compass {
    NORTH,EAST,SOUTH,WEST,NORTHEAST,SOUTHEAST,SOUTHWEST,NORTHWEST;

    private static int[][] CompassMatrix = {
        //x
        {0 , 1, 0, -1, 1, 1, -1, -1},
        //y
        {-1, 0, 1,  0,-1, 1,  1, -1}
    };

    /**
     * Gets a neighbor of the origin tile.
     * @param coordinates the centerpoint (the point returned is w.r.t. the
     *      *        origin)
     * @param direction
     * @return the coordinates (in x and y) of a node neighboring the
     *         given node origin
     */
    public static Coordinates getCoordsWithRespectTo(Coordinates coordinates, Compass direction) {
        int[] coords = new int[2];
        coords[0] = coordinates.getX() + CompassMatrix[0][direction.ordinal()];
        coords[1] = coordinates.getY() + CompassMatrix[1][direction.ordinal()];
        return new Coordinates(coords[0],coords[1]);
    }
}