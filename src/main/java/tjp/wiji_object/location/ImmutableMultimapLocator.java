package tjp.wiji_object.location;

import com.google.common.collect.*;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.drawing.FinalOutputCalculator;
import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.exceptions.TileNotFoundException;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.Correlatable;
import tjp.wiji_object.objects.MapRelativeItem;

import java.util.*;

/**
 * Coordinates with empty lists are different from coordinates without any list.
 */
public final class ImmutableMultimapLocator implements Locator {
    private final ImmutableMultimap<Coordinates, MapRelativeItem> locationMap;
    public static final Builder BUILDER =
            new Builder(ImmutableMultimapLocator::new);

    /**
     * Creates a blank ObjectLocator that represents no locations.
     */
    public ImmutableMultimapLocator() {
        locationMap = ImmutableMultimap.of();
    }

    /**
     *
     * @param locationMap maps map-relative items to coordinates, where the coordinates are in their
     *                    own space and not necessarily in the world coordinate space.
     */
    public ImmutableMultimapLocator(final Map<MapRelativeItem, Coordinates> locationMap) {
        ImmutableMultimap.Builder<Coordinates, MapRelativeItem> builder =
                ImmutableMultimap.builder();
        for(Map.Entry<MapRelativeItem, Coordinates> entry : locationMap.entrySet()) {
            builder.put(entry.getValue(), entry.getKey());
        }
        this.locationMap = builder.build();
    }

    public ImmutableMultimapLocator(final Multimap<Coordinates, MapRelativeItem> locationMap) {
        this.locationMap = ImmutableSetMultimap.copyOf(locationMap);
    }

    public ImmutableMultimapLocator(final MapRelativeItem mapRelativeItem, final Coordinates coords) {
        this(ImmutableMultimap.of(coords, mapRelativeItem));
    }

    @Override
    public Locator copy() {
        SetMultimap<Coordinates, MapRelativeItem> output = HashMultimap.create();
        for (Map.Entry<Coordinates, MapRelativeItem> entry : locationMap.entries()) {
            output.put(entry.getKey(), entry.getValue().copy());
        }
        return new ImmutableMultimapLocator(output);
    }

    @Override
    public Locator mergingUnion(final Locator otherObjectLocator) {
        return new ImmutableMultimapLocator(mergeSimilarValues(
                this.locationMap,
                otherObjectLocator.getLocationMap()));
    }

    @Override
    public Locator destructiveOverride(final Locator otherObjectLocator) {
        return new ImmutableMultimapLocator(destructiveUnion(
                this.locationMap,
                otherObjectLocator.getLocationMap()
        ));
    }

    @Override
    public Locator symmetricDifference(final Locator otherObjectLocator) {
        return new ImmutableMultimapLocator(symmetricDifference(
                this.locationMap,
                otherObjectLocator.getLocationMap()));
    }

    /**
     * A (minuend) - B (subtrahend)
     * All of the entries of the minuend which are not in the subtrahend.
     * @param minuend
     * @param subtrahend
     * @return
     */
    private static ImmutableMultimap<Coordinates, MapRelativeItem> minus(
            final Multimap<Coordinates, MapRelativeItem> minuend,
            final Multimap<Coordinates, MapRelativeItem> subtrahend) {

        final Multimap<Coordinates, MapRelativeItem> removedMap = HashMultimap.create();
        for(Map.Entry<Coordinates, MapRelativeItem> entry : minuend.entries()) {
            if(!subtrahend.values().contains(entry.getValue())) {
                removedMap.put(entry.getKey(), entry.getValue());
            }
        }
        return ImmutableSetMultimap.<Coordinates, MapRelativeItem>builder()
                .putAll(removedMap).build();
    }

    private static ImmutableMultimap<Coordinates, MapRelativeItem> symmetricDifference(
            final Multimap<Coordinates, MapRelativeItem> mapA,
            final Multimap<Coordinates, MapRelativeItem> mapB) {
        final ImmutableMultimap.Builder<Coordinates, MapRelativeItem> outputBuilder =
                ImmutableMultimap.builder();

        Multimap<Coordinates, MapRelativeItem> union = mergeSimilarValues(mapA, mapB);

        for (Map.Entry<Coordinates, Collection<MapRelativeItem>> entry : union.asMap().entrySet()) {
            Coordinates coords = entry.getKey();
            Collection<MapRelativeItem> items = entry.getValue();

            // If there are any non-blocking items...
            if (items.stream().anyMatch((item) -> !item.isBlocking())) {
                // Pull out a random non-blocking item and put it in the output
                outputBuilder.put(coords, items.stream().filter((item) -> !item.isBlocking()).findAny().get());
            } else {
                outputBuilder.putAll(coords, items);
            }
        }

        return outputBuilder.build();
    }

    /**
     * Returns a new multimap with all of the elements of mapA and all of the elements of mapB
     * with two caveats:
     *  - when there are blocking tjp.wiji_object.objects at a tjp.wiji_object.location, remove ALL other tjp.wiji_object.objects
     *  - when there are SIMILAR tjp.wiji_object.objects (like floors that are identical save their UUID), only
     *  return the item from mapB
     * @param mapA a multimap
     * @param mapB a multimap
     * @return a multimap which is the result of a destructive union operation between mapA and mapB
     */
    private static ImmutableMultimap<Coordinates, MapRelativeItem> destructiveUnion(
            final Multimap<Coordinates, MapRelativeItem> mapA,
            final Multimap<Coordinates, MapRelativeItem> mapB) {
        final ImmutableMultimap.Builder<Coordinates, MapRelativeItem> outputBuilder =
                ImmutableMultimap.builder();

        final Multimap<Coordinates, MapRelativeItem> merged = mergeSimilarValues(mapA, mapB);
        for (Map.Entry<Coordinates, Collection<MapRelativeItem>> entry : merged.asMap().entrySet()) {
            Coordinates coords = entry.getKey();
            Collection<MapRelativeItem> items = entry.getValue();

            // If there are any blocking items...
            if (items.stream().anyMatch(MapRelativeItem::isBlocking)) {
                // Pull out a random blocking item and put it in the output
                outputBuilder.put(coords, items.stream().filter(MapRelativeItem::isBlocking).findAny().get());
            } else {
                outputBuilder.putAll(coords, items);
            }
        }
        return outputBuilder.build();
    }

    private static ImmutableMap<MapRelativeItem,Coordinates> deconflict(
            Map<MapRelativeItem,Coordinates> mapA,
            Map<MapRelativeItem,Coordinates> mapB) {
        return ImmutableMap.<MapRelativeItem,Coordinates>builder()
                .putAll(mapA)
                .putAll(mapB)
                .build();
    }

    static <T extends Correlatable> Set<T> reduce(final Iterable<T> items) {
        final HashSet<T> outputSet = new HashSet<>();

        for (T item : items) {
            if (outputSet.stream().noneMatch(item::similarTo)) {
                outputSet.add(item);
            }
        }
        return outputSet;
    }

    /**
     * Merge the two maps but when there are two tjp.wiji_object.objects with similarity in the map, only use one
     * random copy.
     * Not Symmetric!
     * @param mapA
     * @param mapB
     * @param <K>
     * @param <V>
     * @return
     */
    private static <K,V extends Correlatable> ImmutableMultimap<K,V> mergeSimilarValues(
            final Multimap<K, V> mapA, final Multimap<K,V> mapB) {

        final HashMultimap<K,V> combined = HashMultimap.create();
        combined.putAll(mapA);
        combined.putAll(mapB);

        final ImmutableMultimap.Builder<K, V> output = ImmutableMultimap.builder();

        for (Map.Entry<K, Collection<V>> entry : combined.asMap().entrySet()) {
            K key = entry.getKey();
            Collection<V> values = entry.getValue();
            if (values.size() <= 1) {
                output.put(key, entry.getValue().iterator().next());
            } else {
                output.putAll(key, reduce(values));
            }
        }
        return output.build();
    }

    /**
     * Just a dumb straight up merge, not actually used that often. Consider mergeSimilarValues.
     * @param mapA
     * @param mapB
     * @param <K>
     * @param <V>
     * @return
     */
    private static <K,V> ImmutableMultimap<K,V> mergeValues(final Multimap<K, V> mapA, final Multimap<K,V> mapB) {
        return ImmutableSetMultimap.<K,V>builder().putAll(mapA).putAll(mapB).build();
    }

    private boolean hasTile(Coordinates coords) {
        return locationMap.keySet().contains(coords);
    }

    @Override
    public boolean hasBlockingObject(Coordinates location) throws TileNotFoundException {
        return hasBlockingObject(this.locationMap, location);
    }

    @Override
    public boolean hasObject(Coordinates location) throws TileNotFoundException {
        throw new UnsupportedOperationException();
    }

    private static boolean hasBlockingObject(final Multimap<Coordinates, MapRelativeItem> locationMap,
                                             final Coordinates location) {

        return locationMap.get(location).stream().anyMatch(MapRelativeItem::isBlocking);
    }

    @Override
    public Optional<Collection<MapRelativeItem>> getMapRelativeItems(final int x, final int y) {
        return Optional.of(getMapRelativeItems(new Coordinates(x, y)));
    }

    @Override
    public Collection<MapRelativeItem> getMapRelativeItems(Coordinates inCoordinates) {
        return locationMap.get(inCoordinates);
    }

    @Override
    public Optional<ImageRepresentation> getFinalOutput(final int x, final int y) {
        return getFinalOutput(new Coordinates(x, y));
    }

    @Override
    public Optional<ImageRepresentation> getFinalOutput(final Coordinates coords) {
        return FinalOutputCalculator.getFinalOutput(getMapRelativeItems(coords));
    }

    @Override
    public ImmutableMultimapLocator makeNewWithAdded(final MapRelativeItem newGameObject,
                                                     final Coordinates newCoords) {
        return new ImmutableMultimapLocator(mergeSimilarValues(locationMap,
                ImmutableMultimap.of(newCoords, newGameObject)));
    }

    @Override
    public ImmutableMultimapLocator translate(final int x, final int y) {
        return translate(new Coordinates(x,y));
    }

    @Override
    public ImmutableMultimapLocator translate(final Coordinates translationVector) {
        final Multimap<Coordinates, MapRelativeItem> retVal = HashMultimap.create();
        for (Map.Entry<Coordinates, MapRelativeItem> entry : locationMap.entries()) {
             retVal.put(entry.getKey().translate(translationVector), entry.getValue());
        }
        return new ImmutableMultimapLocator(retVal);
    }

    /**
     * TODO: this is O(N), could make this constant time access, but would need to rewrite this
     *       class I believe.
     * @param gameObject
     * @return
     */
    @Override
    public Coordinates locate(final MapRelativeItem gameObject) throws ObjectNotFoundException {
        for (final Coordinates coords :locationMap.keySet()) {
            if(locationMap.get(coords).contains(gameObject)) {
                return coords;
            }
        }

        throw new ObjectNotFoundException();
    }

    @Override
    public ImmutableMultimap<Coordinates, MapRelativeItem> getLocationMap() {
        return ImmutableMultimap.copyOf(locationMap);
    }

    @Override
    public ImmutableSet<Coordinates> getCoordinates() {
        return ImmutableSet.copyOf(locationMap.keySet());
    }
}