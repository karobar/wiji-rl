package tjp.wiji_object.location;

import com.google.common.annotations.Beta;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.*;
import com.google.common.graph.*;
import com.google.errorprone.annotations.Immutable;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.drawing.FinalOutputCalculator;
import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.exceptions.TileNotFoundException;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.MapRelativeItem;
import tjp.wiji_object.objects.MergeClass;
import tjp.wiji_object.objects.Mergeable;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
@Beta
/*
 * Not Finished. Don't use.
 */
public class ImmutableGraphLocator implements Locator {
    // ALL successors should be MapRelativeItems, and ALL predecessors should be Coordinates
    // Also, there shouldn't be any straggler nodes
    private final ImmutableGraph<Mergeable> delegate;
    // This should be a view of delegate.
    private final ImmutableSet<Coordinates> coordinates;
    public static final Builder BUILDER =
            new Builder(ImmutableGraphLocator::new);

    public ImmutableGraphLocator() {
        this(GraphBuilder.directed().<Mergeable>immutable().build());
    }

    private void validateInvariants() {
        for (final Mergeable node : delegate.nodes()) {
            final Set<Mergeable> successors = delegate.successors(node);
            if (!successors.isEmpty()) {
                assert node instanceof Coordinates;
                assert !successors.stream().anyMatch(x-> x instanceof Coordinates);
            }
        }

        assert delegate.isDirected();
    }

    @Override
    public final ImmutableSet<Coordinates> getCoordinates() {
        return coordinates;
    }

    /**
     *
     * @param item
     * @param coords
     */
    public ImmutableGraphLocator(final MapRelativeItem item, final Coordinates coords) {
        this(GraphBuilder.directed().<Mergeable>immutable()
            .putEdge(coords, item)
            .build());
    }

    /**
     * If you want to make this public make sure the invariant holds on the incoming graph!
     * (ALL successors should be MapRelativeItems, and ALL predecessors should be Coordinates)
     */
    private ImmutableGraphLocator(final ImmutableGraph<Mergeable> inGraph) {
        this.delegate = inGraph;

        ImmutableSet.Builder<Coordinates> setBuilder = ImmutableSet.builder();
        for(Mergeable node : delegate.nodes()) {
            if (!delegate.successors(node).isEmpty()) {
                setBuilder.add((Coordinates) node);
            }
        }
        this.coordinates = setBuilder.build();

        validateInvariants();
    }

    public static ImmutableGraphLocator fromMultimap(
            final Multimap<Coordinates, MapRelativeItem> map) {
        checkNotNull(map);
        final MutableGraph<Mergeable> graph = GraphBuilder.directed().build();

        for (Map.Entry<Coordinates, MapRelativeItem> entry : map.entries()) {
            graph.putEdge(entry.getKey(), entry.getValue());
        }
        final ImmutableGraphLocator locator =
                new ImmutableGraphLocator(ImmutableGraph.copyOf(graph));
        return locator;
    }

    @Override
    public Locator copy() {
        final ImmutableGraph.Builder<Mergeable> builder = GraphBuilder.directed().immutable();

        for (final EndpointPair<Mergeable> traversable : delegate.edges()) {
            builder.putEdge(traversable);
        }

        return new ImmutableGraphLocator(builder.build());
    }

    private static Multimap<MergeClass, Mergeable> toMultimap(final Set<Mergeable> toSort) {
        final Multimap<MergeClass, Mergeable> outMap = ArrayListMultimap.create();
        for(final Mergeable toBeMerged : toSort) {
            outMap.put(toBeMerged.get(), toBeMerged);
        }
        return outMap;
    }

    /**
     * Add all from A, overwrites B!
     * @param coords
     * @param itemSetA
     * @param itemSetB
     * @return
     */
    @VisibleForTesting
    static Graph<Mergeable> mergeSimilarValues(
            final Mergeable coords, final Set<Mergeable> itemSetA,
            final Set<Mergeable> itemSetB) {
        final MutableGraph<Mergeable> outGraph = GraphBuilder.directed().build();

        itemSetA.forEach((Mergeable item) ->
                outGraph.putEdge(coords, item));

        // Preheat the oven by transforming setB to a Multimap
        final Multimap<MergeClass, Mergeable> multimapB = toMultimap(itemSetB);
        multimapB.get(MergeClass.UNMERGE).forEach(
                (Mergeable item) ->
                        outGraph.putEdge(coords, item));

        final Set<MergeClass> aClasses = itemSetA.stream()
                .map(Mergeable::get)
                .collect(Collectors.toSet());


        // B - A
        final Sets.SetView<MergeClass> intermediate = Sets.difference(multimapB.keySet(), aClasses);
        // we've already handled UNMERGE, don't do it again
        final Sets.SetView<MergeClass> after = Sets.difference(
                intermediate, ImmutableSet.of(MergeClass.UNMERGE));

        after.forEach((MergeClass category) ->
            // We only want the first of each multimap anyway
            outGraph.putEdge(coords, multimapB.get(category).iterator().next()));

        return outGraph;
    }

    /**
     * Adds all from A, overwrites B!
     * @param graphA
     * @param graphB
     * @return
     */
    @VisibleForTesting
    static ImmutableGraph<Mergeable> mergeSimilarValues(
            Graph<Mergeable> graphA,
            Graph<Mergeable> graphB) {
        assert graphA.isDirected();
        assert graphB.isDirected();

        final ImmutableGraph.Builder<Mergeable> builder = GraphBuilder.directed().immutable();
        for (final Mergeable nodeA : graphA.nodes()) {
            if (!graphA.successors(nodeA).isEmpty()) {
                // if nodeA has successors, nodeA must be a Coordinates
                assert nodeA instanceof Coordinates;

                final Set<Mergeable> aSuccessors = graphA.successors(nodeA);
                final Set<Mergeable> bSuccessors = graphB.successors(nodeA);

                mergeSimilarValues(nodeA,aSuccessors,bSuccessors)
                        .edges().stream()
                        .forEach(builder::putEdge);
            }
        }
        return builder.build();
    }

    @Override
    public Locator mergingUnion(final Locator otherObjectLocator) {
        return new ImmutableGraphLocator(
            mergeSimilarValues(
                        delegate,
                        ImmutableGraphLocator.fromMultimap(
                                otherObjectLocator.getLocationMap()).delegate)
        );
    }

    ImmutableGraph<Mergeable> overwriteBValues(final Locator graphB) {

        final ImmutableGraph.Builder<Mergeable> builder = GraphBuilder.directed().immutable();

        // Add everything from A
        for (final Coordinates node : getCoordinates()) {
            getMapRelativeItems(node)
                    .forEach(item-> builder.putEdge(node, item));
        }
        // Add everything from B - A
        final ImmutableSet<Coordinates> bMinusA = Sets.difference(
                graphB.getCoordinates(), getCoordinates()).immutableCopy();
        // Add everything from A
        for (final Coordinates node : bMinusA) {
            graphB.getMapRelativeItems(node)
                    .forEach(item-> builder.putEdge(node, item));
        }

        return builder.build();
    }

    @Override
    public Locator destructiveOverride(final Locator otherObjectLocator) {
        return new ImmutableGraphLocator(
                overwriteBValues(otherObjectLocator));
    }

    @Override
    public Locator symmetricDifference(Locator otherObjectLocator) {
        throw new NotImplementedException();
    }

    @Override
    public boolean hasBlockingObject(final Coordinates location) throws TileNotFoundException {
        if (delegate.nodes().contains(location)) {
            return delegate.successors(location).stream()
                    // This risks a conversion runtime error, but all successors SHOULD be
                    // MapRelativeItems
                    .map((Mergeable traversable) -> (MapRelativeItem) traversable)
                    .anyMatch(MapRelativeItem::isBlocking);
        }
        throw new TileNotFoundException("This object locator doesn't contain that location");
    }

    @Override
    public boolean hasObject(final Coordinates location) throws TileNotFoundException {
        if (delegate.nodes().contains(location)) {
            return delegate.successors(location).size() > 0;
        }
        throw new TileNotFoundException("This object locator doesn't contain that location");
    }

    @Override
    public Optional<Collection<MapRelativeItem>> getMapRelativeItems(int x, int y) {
        return Optional.of(getMapRelativeItems(new Coordinates(x,y)));
    }

    @Override
    public Collection<MapRelativeItem> getMapRelativeItems(final Coordinates inCoordinates) {
        for(final Mergeable node : delegate.nodes()) {
            if (node.equals(inCoordinates)){

                Set<MapRelativeItem> mapRelativeItems = new HashSet<>();

                mapRelativeItems.addAll(delegate.successors(node).stream()
                        .map(x-> (MapRelativeItem) x)
                        .collect(Collectors.toSet()));
                return mapRelativeItems;
            }
        }
        return new HashSet<>();
    }

    @Override
    public Optional<ImageRepresentation> getFinalOutput(final int x, final int y) {
        return getFinalOutput(new Coordinates(x, y));
    }

    @Override
    public Optional<ImageRepresentation> getFinalOutput(final Coordinates coords) {
        return FinalOutputCalculator.getFinalOutput(delegate.successors(coords).stream()
                // This risks a conversion runtime error, but all successors SHOULD be
                // MapRelativeItems
                .map((Mergeable traversable) -> (MapRelativeItem) traversable)
                .collect(Collectors.toSet()));
    }

    @Override
    public Locator makeNewWithAdded(final MapRelativeItem newGameObject,
                                    final Coordinates newCoords) {
        final ImmutableGraph.Builder<Mergeable> builder = GraphBuilder.directed().immutable();
        for (final EndpointPair<Mergeable> edge : delegate.edges()){
            builder.putEdge(edge);
        }
        builder.putEdge(newCoords, newGameObject);

        return new ImmutableGraphLocator(builder.build());
    }

    @Override
    public Locator translate(int x, int y) {
        throw new NotImplementedException();
    }

    @Override
    public Locator translate(Coordinates translationVector) {
        throw new NotImplementedException();
    }

    @Override
    public Coordinates locate(MapRelativeItem gameObject) throws ObjectNotFoundException {
        // Assumes there is only on predecessor and that the predecessor is a Coordinates
        // These assumptions are made due to the invariant defined in this class
        return (Coordinates) delegate.predecessors(gameObject).iterator().next();
    }

    @Override
    public ImmutableMultimap<Coordinates, MapRelativeItem> getLocationMap() {
        final SetMultimap<Coordinates, MapRelativeItem> outMap = HashMultimap.create();

        for (final Mergeable node : delegate.nodes()) {
            final Set<Mergeable> successors = delegate.successors(node);
            if (!successors.isEmpty()) {
                // Guaranteed by the invariants guaranteed by this class that this node will be a
                // Coordinates
                successors.forEach((Mergeable successor) -> outMap.put(
                        (Coordinates) node, (MapRelativeItem) successor));
            }

        }
        return ImmutableMultimap.copyOf(outMap);
    }
}
