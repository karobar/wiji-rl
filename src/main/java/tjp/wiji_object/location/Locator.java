package tjp.wiji_object.location;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.exceptions.TileNotFoundException;
import tjp.wiji_object.generation.Builder;
import tjp.wiji_object.objects.MapRelativeItem;

import java.util.Collection;
import java.util.Optional;

public interface Locator {
    Locator copy();

    /**
     * Given an objectLocator, merge it with this ObjectLocator. This ObjectLocator Overwrites
     * the other ObjectLocator's blocking tjp.wiji_object.objects
     * @param otherObjectLocator
     * @return
     */
    Locator mergingUnion(Locator otherObjectLocator);

    /**
     * Merges with the ObjectLocator but removes ALL tjp.wiji_object.objects where there are things in this object
     * locator. Use wisely.
     * An example where you might want to use this is to lay floors that span an area, and then
     * build walls that run the perimeter and destroy the floor tiles that would be underneath
     * the walls.
     * @return
     */
    Locator destructiveOverride(Locator otherObjectLocator);

    /**
     * https://en.wikipedia.org/wiki/Symmetric_difference
     * basically a union but
     * @param otherObjectLocator
     * @return
     */
    Locator symmetricDifference(Locator otherObjectLocator);

    boolean hasBlockingObject(Coordinates location) throws TileNotFoundException;

    boolean hasObject(final Coordinates location) throws TileNotFoundException;

    /**
     * TODO: under what circumstances is this an empty collection, and when is it absent?
     * @param x
     * @param y
     * @return
     */
    Optional<Collection<MapRelativeItem>> getMapRelativeItems(int x, int y);

    /**
     *
     * @param inCoordinates
     * @return The coordinates may not be registered with this locator, in which case, returns
     * Optional.empty(). If the coordinates are registered but the coordinates are empty, return
     * an empty collection.
     */
    Collection<MapRelativeItem> getMapRelativeItems(Coordinates inCoordinates);

    Optional<ImageRepresentation> getFinalOutput(int x, int y);

    /**
     * Given some coordinates, return the image representation that square should have.
     * @param coords
     * @return
     */
    Optional<ImageRepresentation> getFinalOutput(Coordinates coords);

    /**
     * Creates a new object at the given Coordinates.
     * @param newGameObject
     * @param newCoords
     */
    Locator makeNewWithAdded(MapRelativeItem newGameObject,
                             Coordinates newCoords);

    Locator translate(int x, int y);

    Locator translate(Coordinates translationVector);

    Coordinates locate(MapRelativeItem gameObject) throws ObjectNotFoundException;

    ImmutableMultimap<Coordinates, MapRelativeItem> getLocationMap();

    ImmutableSet<Coordinates> getCoordinates();


}
