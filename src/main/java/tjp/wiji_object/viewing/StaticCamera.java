package tjp.wiji_object.viewing;

import tjp.wiji_object.location.Coordinates;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.RepresentationField;

import java.util.Optional;

public class StaticCamera implements RepresentationField {
    private final RepresentationField field;
    private final Coordinates origin;

    public StaticCamera(final RepresentationField field,
                        final Coordinates origin) {
        this.field = field;
        this.origin = origin;
    }

    public StaticCamera(final RepresentationField field) {
        this(field, new Coordinates(0,0));
    }

    @Override
    public Optional<ImageRepresentation> get(int x, int y) {
        return field.get(x + origin.getX(), y + origin.getY());
    }
}
