package tjp.wiji_object.generation;

import com.google.common.collect.ImmutableList;
import com.google.errorprone.annotations.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tjp.wiji_object.location.Coordinates;

import tjp.wiji_object.location.Locator;
import tjp.wiji_object.objects.GameObject;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
public class Builder {
    private static final Logger LOGGER = LoggerFactory.getLogger(Builder.class);

    private final Supplier<Locator> objectLocatorFactory;

    public Builder(final Supplier<Locator> objectLocatorFactory) {
        this.objectLocatorFactory = checkNotNull(objectLocatorFactory);
    }

    /**
     * Given a start coordinate, an end coordinate, and a wallType, run a wall from start to end.
     * @param start
     * @param end
     * @param wallType
     * @return
     */
    private Locator runWall(final Coordinates start, final Coordinates end,
                                   final GameObject.Factory wallType)  {
        Locator placements = objectLocatorFactory.get();

        int smallX = Math.min(start.getX(), end.getX());
        int bigX = Math.max(start.getX(), end.getX());
        int smallY = Math.min(start.getY(), end.getY());
        int bigY = Math.max(start.getY(), end.getY());
        int startX = smallX;

        if((start.getX() != end.getX()) && (start.getY() != end.getY())) {
            throw new UnsupportedOperationException(String.format(
                    "createLineWall does not currently support diagonal walls. " +
                            "You tried creating a wall from point (%s,%s) to (%s,%s).",
                    start.getX(), start.getY(), end.getX(), end.getY()));
        }
        while(smallY <= bigY) {
            while(smallX <= bigX) {
                final Coordinates desiredCoordinates = new Coordinates(smallX, smallY);
                System.out.printf("Adding at %s\n", desiredCoordinates);
                placements = placements.makeNewWithAdded(wallType.make(),
                            desiredCoordinates);
                smallX++;
            }
            smallY++;
            smallX = startX;
        }
        return placements;
    }

    public Locator buildSquare(final GameObject.Factory wallFactory,
                                      final GameObject.Factory floorFactory,
                                      final int width) {
        return buildRectangle(wallFactory, floorFactory, width, width);
    }

    public Locator buildRectangle(final GameObject.Factory wallFactory,
                                         final GameObject.Factory floorFactory,
                                         final int width, final int height) {
        final ImmutableList<Coordinates> vertices = ImmutableList.of(
                new Coordinates(0,0),
                new Coordinates(width-1,0),
                new Coordinates(width-1, height-1),
                new Coordinates(0, height-1)
        );

        return buildClosedPolygon(wallFactory, floorFactory, vertices);
    }

    /**
     * TODO: make this more intelligent than rectangular.
     * @param floorFactory
     * @param coordinates
     * @return
     */
    private Locator createFloors(final GameObject.Factory floorFactory,
                                        final List<Coordinates> coordinates) {
        checkNotNull(floorFactory);

        Locator floors = objectLocatorFactory.get();
        int leftX, topY, rightX, bottomY;
        try {
            leftX = coordinates.stream()
                    .min(Comparator.comparing(Coordinates::getX))
                    .orElseThrow(NoSuchElementException::new)
                    .getX();
            topY = coordinates.stream()
                    .min(Comparator.comparing(Coordinates::getY))
                    .orElseThrow(NoSuchElementException::new)
                    .getY();
            rightX = coordinates.stream()
                    .max(Comparator.comparing(Coordinates::getX))
                    .orElseThrow(NoSuchElementException::new)
                    .getX();
            bottomY = coordinates.stream()
                    .max(Comparator.comparing(Coordinates::getY))
                    .orElseThrow(NoSuchElementException::new)
                    .getY();
        } catch (NoSuchElementException e) {
            return objectLocatorFactory.get();
        }

        for (int x = leftX; x <= rightX; x++) {
            for (int y = topY; y <= bottomY; y++) {
                floors = floors.makeNewWithAdded(floorFactory.make(),
                        new Coordinates(x, y));
            }
        }
        return floors;
    }

    private Locator runWalls(final GameObject.Factory wallFactory,
                                    final List<Coordinates> coordinates) {
        Locator polygon = objectLocatorFactory.get();

        Coordinates firstVertex = null;
        Coordinates prevVertex = null;
        Coordinates currentVertex = null;
        for (Coordinates coordinate : coordinates) {
            currentVertex = coordinate;

            if (firstVertex == null) {
                firstVertex = currentVertex;
                prevVertex = currentVertex;
            } else {
                Locator newObjs = runWall(prevVertex, currentVertex, wallFactory);
                polygon = polygon.mergingUnion(newObjs);
                prevVertex = coordinate;
            }
        }
        // Run one last wall to connect up to the first one
        return polygon.mergingUnion(runWall(currentVertex, firstVertex, wallFactory));
    }

    public Locator buildClosedPolygon(final GameObject.Factory wallFactory,
                                             final GameObject.Factory floorFactory,
                                             final List<Coordinates> coordinates) {
        Locator polygon = objectLocatorFactory.get();
        if (coordinates.size() > 0) {
            final Locator floors = createFloors(floorFactory, coordinates);
            final Locator walls = runWalls(wallFactory, coordinates);

            polygon = walls.destructiveOverride(floors);
        }
        return polygon;
    }
}
