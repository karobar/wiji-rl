package tjp.wiji_object.objects;

/**
 * Can be checked for similarity.
 */
public interface Correlatable {
    /**
     * Compares this Similarable
     * @param other
     * @return
     */
    boolean similarTo(Object other);
}
