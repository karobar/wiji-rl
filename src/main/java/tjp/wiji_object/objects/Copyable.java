package tjp.wiji_object.objects;

public interface Copyable<T> {
    T copy();
}
