package tjp.wiji_object.objects;

public enum MergeClass {
    FLOOR, WALL,
    /**
     * Don't merge UNMERGE values
     */
    UNMERGE;
}
