/*
 * Copyright 2013 Travis Pressler
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *
 * GameObject.java
 */
package tjp.wiji_object.objects;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.errorprone.annotations.Immutable;
import tjp.wiji_object.drawing.ObjectGeometry;
import tjp.wiji_object.drawing.PrecedenceClass;
import tjp.wiji_object.location.Compass;
import tjp.wiji.representations.Graphic;

import java.awt.*;
import java.util.Optional;
import java.util.Stack;
import java.util.UUID;

/**
 * Parent type of PlacedObject and Factory.
 *
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
@Immutable
public class GameObject implements MapRelativeItem, Correlatable {

    /**
     * Makes GameObjects.
     */
    public static final class Factory {
        private final String name;
        private final ObjectGeometry sprite;
        private final boolean isBlocking;
        private final boolean isGrabbable;
        private final PrecedenceClass precedenceClass;

        /**
         * Creates GameObjects with default precedence.
         */
        public Factory(final String name, ObjectGeometry sprite, boolean isBlocking,
                       boolean isGrabbable) {
            this(name, sprite, isBlocking, isGrabbable,
                    PrecedenceClass.DEFAULT);
        }

        /**
         * Creates GameObjects.
         * @param name
         * @param sprite
         * @param isBlocking
         * @param isGrabbable
         * @param precedenceClass
         */
        public Factory(final String name, ObjectGeometry sprite, boolean isBlocking,
                       boolean isGrabbable, PrecedenceClass precedenceClass) {
            this.name = name;
            this.sprite = sprite;
            this.isBlocking = isBlocking;
            this.isGrabbable = isGrabbable;
            this.precedenceClass = precedenceClass;
        }

        public GameObject make() {
            return new GameObject(
                    name,
                    sprite,
                    isBlocking,
                    isGrabbable);
        }
    }

    private final boolean blocking;
    public final boolean grabbable;
    public final ObjectGeometry sprite;
    public final String name;
    //In its current implementation, desires will be filled with tiles;
    private final Stack<Compass> desires = new Stack<Compass>();
    private final String detailedDescription;
    private final UUID uuid;
    private final PrecedenceClass precedenceClass;
    private final MergeClass mergeClass;

    /**
     * Factory constructor without a Location for object templates only. Uses the default
     * precedence class
     */
    public GameObject(
            String name,
            ObjectGeometry sprite,
            boolean blocking,
            boolean grabbable) {
        this(name,sprite,blocking, grabbable,
                PrecedenceClass.DEFAULT, MergeClass.UNMERGE);
    }

    /**
     * Factory constructor without a Location for object templates only
     */
    public GameObject(
            String name,
            ObjectGeometry sprite,
            boolean blocking,
            boolean grabbable,
            PrecedenceClass precedenceClass) {
        this(name,sprite,blocking, grabbable,
                precedenceClass, MergeClass.UNMERGE);
    }

    /**
     * Factory constructor without a Location for object templates only
     */
    public GameObject(
            String name,
            ObjectGeometry sprite,
            boolean blocking,
            boolean grabbable,
            PrecedenceClass precedenceClass,
            MergeClass mergeClass) {
        this.blocking = blocking;
        this.grabbable = grabbable;
        this.sprite = sprite;
        this.name = name;
        this.detailedDescription = "This is a " + name;
        this.uuid = UUID.randomUUID();
        this.precedenceClass = precedenceClass;
        this.mergeClass = mergeClass;
    }

    @Override
    public GameObject copy() {
        return new GameObject(
                this.name,
                this.sprite,
                this.blocking,
                this.grabbable,
                this.precedenceClass,
                mergeClass
        );
    }

    private boolean similarityCriteria(final GameObject other) {
        return Objects.equal(this.blocking, other.blocking)
                && Objects.equal(this.grabbable, other.grabbable)
                && Objects.equal(this.sprite, other.sprite)
                && Objects.equal(this.name, other.name)
                && Objects.equal(this.desires, other.desires)
                && Objects.equal(this.detailedDescription, other.detailedDescription)
                && Objects.equal(this.precedenceClass, other.precedenceClass);
    }

    @Override
    public Color getForeColor() {
        return sprite.getForeColor();
    }

    @Override
    public Optional<Color> getBackColor() {
        return sprite.getBackColor();
    }

    @Override
    public Graphic getImgChar() {
        return sprite.getGraphic();
    }

    @Override
    public boolean isBlocking() {
        return blocking;
    }

    public PrecedenceClass getPrecedenceClass() {
        return this.precedenceClass;
    }

    @Override
    public MergeClass get() {
        return mergeClass;
    }

    /**
     * Compares this to another object
     */
    @Override
    public boolean similarTo(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        return similarityCriteria((GameObject) obj);

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        GameObject other = (GameObject) obj;
        return similarityCriteria(other)
                && Objects.equal(this.uuid, other.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                blocking, grabbable, sprite, name, desires,detailedDescription, uuid,
                precedenceClass);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("blocking", blocking)
                .add("grabbable", grabbable)
                .add("sprite", sprite)
                .add("name", name)
                .add("desires", desires)
                .add("detailedDescription", detailedDescription)
                .add("uuid", uuid)
                .add("precedenceClass", precedenceClass)
                .toString();
    }
}