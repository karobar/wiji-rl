package tjp.wiji_object.objects;

import java.util.function.Supplier;

public interface Mergeable extends Supplier<MergeClass> { }
