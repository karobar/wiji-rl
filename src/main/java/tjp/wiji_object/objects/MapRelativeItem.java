package tjp.wiji_object.objects;

import tjp.wiji.representations.Graphic;
import tjp.wiji_object.drawing.PrecedenceClass;

import java.awt.*;
import java.util.Optional;

public interface MapRelativeItem extends Copyable<MapRelativeItem>, Correlatable,
        Mergeable {
    Color getForeColor();

    Optional<Color> getBackColor();

    Graphic getImgChar();

    boolean isBlocking();

    default PrecedenceClass getPrecedenceClass() {
        return PrecedenceClass.DEFAULT;
    }
}
