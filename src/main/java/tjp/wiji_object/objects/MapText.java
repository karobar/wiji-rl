package tjp.wiji_object.objects;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import tjp.wiji.representations.Graphic;

import java.awt.*;
import java.util.Optional;

public class MapText implements MapRelativeItem {
    @Override
    public MapRelativeItem copy() {
        throw new NotImplementedException();
    }

    /**
     * Compares this Similarable
     *
     * @param other
     * @return
     */
    @Override
    public boolean similarTo(Object other) {
        return false;
    }

    @Override
    public Color getForeColor() {
        return null;
    }

    @Override
    public Optional<Color> getBackColor() {
        return Optional.empty();
    }

    @Override
    public Graphic getImgChar() {
        return null;
    }

    @Override
    public boolean isBlocking() {
        return false;
    }

    @Override
    public MergeClass get() {
        return MergeClass.UNMERGE;
    }
}
