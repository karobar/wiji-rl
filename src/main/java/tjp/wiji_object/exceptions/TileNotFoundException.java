package tjp.wiji_object.exceptions;

public class TileNotFoundException extends Exception {
    public TileNotFoundException() {
    }

    public TileNotFoundException(String message) {
        super(message);
    }
}
