package tjp.wiji_object.drawing;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import tjp.wiji.representations.Graphic;
import javax.annotation.Nullable;
import java.awt.*;
import java.util.Optional;

public final class ObjectGeometry {
    public static final ObjectGeometry ERROR = new ObjectGeometry(
            Color.MAGENTA, Color.GREEN, Graphic.QUESTION_MARK);

    private final Color foreColor;
    @Nullable
    private final Color backColor;
    private final Graphic graphic;

    public ObjectGeometry(final Color foreColor, final Color backColor,
                          final Graphic graphic) {
        this.foreColor = foreColor;
        this.backColor = backColor;
        this.graphic = graphic;
    }

    public Graphic getGraphic() {
        return graphic;
    }

    public Optional<Color> getBackColor() {
        return Optional.ofNullable(backColor);
    }

    public Color getForeColor() {
        return foreColor;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        ObjectGeometry other = (ObjectGeometry) obj;
        return Objects.equal(this.backColor, other.backColor)
                && Objects.equal(this.foreColor, other.foreColor)
                && Objects.equal(this.graphic, other.graphic);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                backColor, foreColor, graphic
        );
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("foreColor", foreColor)
                .add("backColor", backColor)
                .add("imgChar", graphic)
                .toString();

    }
}