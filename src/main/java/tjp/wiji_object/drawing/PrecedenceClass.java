package tjp.wiji_object.drawing;

/**
 * Precedence defined in classes, tiers
 *
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public enum PrecedenceClass {
    /** Beware, the order of these matters */
    GUI, OVERHANG, KEY_INFORMATION, DEFAULT, FLOOR;

    /**
     *
     * @param other
     * @return a -1 represents that 'this' is less than 'other', a 0 represents
     *         that 'this'='other', a 1 represents that 'this' is greater than
     *         'other'
     */
    public int comparePrecedence(PrecedenceClass other) {
        return Integer.compare(other.ordinal(), this.ordinal());
    }
}
