package tjp.wiji_object.drawing;

import tjp.wiji.drawing.CellExtents;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.RepresentationField;
import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.location.Coordinates;
import tjp.wiji_object.objects.MapRelativeItem;
import tjp.wiji_object.viewing.StaticCamera;

import java.util.Optional;

/**
 * It's like the map and the camera at the same time or something. Maybe all this should be rolled
 * into GameMap?
 */
public class MapContext implements RepresentationField {
    public final CellExtents extents;
    private final GameMap map;
    private final RepresentationField camera;

    public MapContext(final CellExtents extents, final GameMap map,
                      final RepresentationField camera) {
        this.extents = extents;
        this.map = map;
        this.camera = camera;
    }

    public MapContext(final CellExtents extents, final GameMap map) {
        this(extents, map, new StaticCamera(map));
    }

    /**
     *
     * @param screenX
     * @param screenY
     * @return The ImageRepresentation to display IN SCREEN SPACE. It should have been
     * transformed by a camera.
     */
    @Override
    public Optional<ImageRepresentation> get(int screenX, int screenY) {
        return camera.get(screenX, screenY);
    }

    public Coordinates locate(final MapRelativeItem item) throws ObjectNotFoundException {
        return map.locate(item);
    }
}
