package tjp.wiji_object.drawing;

import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.location.Coordinates;
import tjp.wiji_object.objects.MapRelativeItem;

import java.awt.*;
import java.util.Collection;
import java.util.Optional;

public class FinalOutputCalculator {
    private static Optional<ImageRepresentation> getNonEmptyFinalOutput(final Collection<MapRelativeItem> items) {
        MapRelativeItem first = items.iterator().next();
        MapRelativeItem min = first;
        MapRelativeItem max = first;

        boolean hasOverhang = false;
        Color overhangForeColor = Color.WHITE;
        boolean hasKeyInformation = false;
        Color keyInformationBackColor = Color.WHITE;
        for(MapRelativeItem currentObject : items) {
            final PrecedenceClass precedenceClass = currentObject.getPrecedenceClass();

            switch (precedenceClass) {
                case GUI:
                    return Optional.of(new ImageRepresentation(
                            currentObject.getForeColor(),
                            currentObject.getImgChar()));
                case OVERHANG:
                    hasOverhang = true;
                    overhangForeColor = currentObject.getForeColor();
                    break;
                case KEY_INFORMATION:
                    hasKeyInformation = true;
                    keyInformationBackColor = currentObject.getForeColor();
                    break;
                default:
                    break;
            }

            if (precedenceClass.comparePrecedence(min.getPrecedenceClass()) == -1) {
                min = currentObject;
            }
            if (precedenceClass.comparePrecedence(max.getPrecedenceClass()) == 1) {
                max = currentObject;
            }
        }

        Color foreColor;
        Color backColor = null;
        if (hasOverhang && hasKeyInformation) {
            foreColor = overhangForeColor;
            backColor = keyInformationBackColor;
        } else {
            // get the color of the highest-precedence object of the tile
            foreColor = max.getForeColor();

            if (max.getBackColor().isPresent()) {
                backColor = max.getBackColor().get();
            } else {
                // get the background color of the
                // lowest-precedence object in the tile
                Optional<Color> minBackColor = min.getBackColor();
                if (minBackColor.isPresent()) {
                    backColor = minBackColor.get();
                }
            }
        }
        // get the foreground character of the
        // highest-precedence object of the tile
        Graphic imgChar = max.getImgChar();
        if (backColor == null || foreColor == backColor) {
            // something is probably wrong
            backColor = Color.MAGENTA;
        }
        return Optional.of(new ImageRepresentation(foreColor, backColor, imgChar));
    }

    /**
     * Given some coordinates, return the image representation that square should have.
     * @return
     */
    public static Optional<ImageRepresentation> getFinalOutput(
            final Collection<MapRelativeItem> items) {

        if (items.size() > 0) {
            return getNonEmptyFinalOutput(items);
        }
        return Optional.empty();
    }
}
