/**
 * Any files which reference the base Wiji tjp.wiji_object.drawing library should be in this package.
 */
package tjp.wiji_object.drawing;
