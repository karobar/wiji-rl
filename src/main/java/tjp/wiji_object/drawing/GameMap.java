package tjp.wiji_object.drawing;

import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.location.Coordinates;
import tjp.wiji_object.location.Locator;
import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.RepresentationField;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji_object.objects.MapRelativeItem;

import java.awt.*;
import java.util.Optional;

/**
 * The GameMap is a logical representation of a group of
 * Game Objects (objectList)arranged on a two-dimensional array of Tiles (delegate)
 *
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public class GameMap implements RepresentationField {
    private Locator delegate;
    private final int width;
    private final int height;
    private final ImageRepresentation missingTileImage;

    private static ImageRepresentation DEFAULT_MISSING_TILE_IMAGE = new ImageRepresentation(
            Color.GRAY, Color.BLACK, Graphic.CENTERED_DOT);

    public GameMap(final Locator delegate, final int width, final int height) {
        this(delegate, width, height, DEFAULT_MISSING_TILE_IMAGE);
    }

    public GameMap(final Locator delegate, final int width, final int height,
                   final ImageRepresentation missingTileImage) {
        this.delegate = delegate;
        this.width = width;
        this.height = height;
        this.missingTileImage = missingTileImage;
    }

    public void add(final GameObject gameObject, final Coordinates coordinates) {
        this.delegate = delegate.makeNewWithAdded(gameObject, coordinates);
    }

    /**
     * TODO: not even sure this is useful anymore
     * @param x
     * @param y
     * @return
     */
    public boolean isValidTile(int x,int y) {
        return x < width && y < height && x >= 0 && y >= 0;
    }

    /**
     * Returns the object in a specific delegate tile with the smallest
     * precedence.
     * @param x
     * @param y
     * @return
     */
    @Override
    public Optional<ImageRepresentation> get(int x, int y) {
        if (isValidTile(x,y)) {
            return delegate.getFinalOutput(new Coordinates(x,y));
        }
        return Optional.empty();
    }

    public Coordinates locate(final MapRelativeItem item) throws ObjectNotFoundException {
        return delegate.locate(item);
    }
}
